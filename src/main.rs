use axum::{
    body::Body,
    extract::ConnectInfo,
    http::{Request, StatusCode},
    response::IntoResponse,
    routing::get,
    Json, Router,
};
use axum_server::tls_rustls::RustlsConfig;
use axum_server_dual_protocol::ServerExt;

use listenfd::ListenFd;
use std::{
    net::{IpAddr, Ipv4Addr, SocketAddr},
    path::PathBuf,
};

use clap::Parser;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Port to Listen on
    #[arg(short, long, env = "LISTEN_PORT")]
    port: Option<u16>,

    /// IP to bind to
    #[arg(short, long, env = "LISTEN_IP")]
    ip: Option<IpAddr>,

    /// Bind connection, ignoring LISTEN_FD if it's present
    #[arg(long, default_value_t = false)]
    ignore_listen_fds: bool,

    /// Server TLS certificate file
    #[arg(long,
          env,
          default_value_os_t=PathBuf::from(".")
                          .join(".dev-pki")
                          .join("server.crt"),
          value_parser=clap::value_parser!(PathBuf))]
    tls_cert_file: PathBuf,

    /// Server TLS private key file
    #[arg(long,
          env,
          default_value_os_t=PathBuf::from(".")
                          .join(".dev-pki")
                          .join("server.key"),
          value_parser=clap::value_parser!(PathBuf))]
    tls_key_file: PathBuf,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let log_level_env_filter = tracing_subscriber::EnvFilter::try_from_default_env()
        .unwrap_or_else(|_| format!("{}=debug", env!("CARGO_BIN_NAME").replace('-', "_")).into());
    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_env_filter(log_level_env_filter)
        .with_writer(std::io::stderr)
        .finish();

    tracing::subscriber::set_global_default(subscriber)?;

    let args = Args::parse();

    let listen_fd = ListenFd::from_env().take_tcp_listener(0)?;
    if listen_fd.is_some() {
        if args.ip.is_some() {
            tracing::info!("Both LISTEN_FDS and LISTEN_IP provided, ignoring LISTEN_IP");
        }
        if args.port.is_some() {
            tracing::info!("Both LISTEN_FDS and LISTEN_PORT provided, ignoring LISTEN_PORT");
        }
    }
    // configure certificate and private key used by https
    let config = RustlsConfig::from_pem_file(args.tls_cert_file, args.tls_key_file).await?;

    // run https server
    let server = if let Some(listener) = (!args
        .ignore_listen_fds)
        .then_some(listen_fd)
        .flatten()
    {
        tracing::debug!("listening on {:?}", listener);
        axum_server_dual_protocol::from_tcp_dual_protocol(listener, config)
    } else {
        let addr = SocketAddr::from((
            args.ip.unwrap_or(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1))),
            args.port.unwrap_or(3000),
        ));
        tracing::debug!("listening on {}", addr);
        axum_server_dual_protocol::bind_dual_protocol(addr, config)
    };

    let app = Router::new()
        .route("/health", get(health))
        .route("/", get(handler));

    let server_fut = server
        .set_upgrade(true)
        .serve(app.into_make_service_with_connect_info::<SocketAddr>());

    tokio::select! {
        server_result = server_fut => {
            tracing::info!("Server ended");
            server_result?;
        },
        signal_name = get_signal() => {
            tracing::info!("Received {} signal, shutting down", signal_name)
        }
    }

    Ok(())
}

async fn handler(_req: Request<Body>) -> Result<impl IntoResponse, (StatusCode, String)> {
    Ok(Json(()))
}

async fn health(
    ConnectInfo(addr): ConnectInfo<SocketAddr>,
) -> Result<impl IntoResponse, (StatusCode, String)> {
    tracing::info!("Received Health Check from {}", addr);
    Ok(Json("Healthy"))
}

async fn get_signal() -> &'static str {
    use tokio::signal::unix::{signal, SignalKind};
    let mut interrupt_signal = signal(SignalKind::interrupt()).unwrap();
    let mut quit_signal = signal(SignalKind::quit()).unwrap();
    let mut term_signal = signal(SignalKind::terminate()).unwrap();

    tokio::select! {
        _ = interrupt_signal.recv() => { "SIGINT" }
        _ = quit_signal.recv() => { "SIGQUIT"  }
        _ = term_signal.recv() => { "SIGTERM" }
    }
}
